#!/bin/bash
version=${1:-HEAD}
tag=${2:-$1}

export YMTK_PROJECT="bamboo6"
export YMTK_ENVIRONMENT="dev"
export YMTK_REGION=ap-southeast-2
export YMTK_PROFILE=geoop
export AWS_DEFAULT_REGION=${YMTK_REGION}
export AWS_DEFAULT_PROFILE=${YMTK_PROFILE}

ymtk create-stack --template vpc.json --stack "${YMTK_PROJECT,,}-${YMTK_ENVIRONMENT,,}-vpc" --upsert
ymtk wait-create-stack --stack "${YMTK_PROJECT,,}-${YMTK_ENVIRONMENT,,}-vpc"
ymtk create-stack --template factory.json --stack "${YMTK_PROJECT,,}-${YMTK_ENVIRONMENT,,}-factory" --upsert