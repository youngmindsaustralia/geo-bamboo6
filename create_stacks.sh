#!/bin/bash
version=$1
export YMTK_PROJECT="bamboo6"
export YMTK_ENVIRONMENT="dev"
export YMTK_REGION=ap-southeast-2
export YMTK_PROFILE=geoop
export AWS_DEFAULT_REGION=${YMTK_REGION}
export AWS_DEFAULT_PROFILE=${YMTK_PROFILE}

echo "INFO: Looking for AMI \"bamboo6-${version}\" ..."
export imageId=$(aws ec2 describe-images --filter "Name=name,Values=bamboo6-${version}" \
    | jq '.Images[0].ImageId' | sed 's/^"\(.*\)"$/\1/')
if [[ "${ImageId}" == "null" ]]; then
    echo "ERROR: No AMI found for \"portal-${version}\"" >&2
    exit 1
fi

ymtk create-stack --template bamboo.json --stack "${YMTK_PROJECT,,}-${YMTK_ENVIRONMENT,,}-bamboo" --upsert
