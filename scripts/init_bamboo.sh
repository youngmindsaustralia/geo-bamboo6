#!/bin/bash

hostname="bamboo6.jobtrakka.com"
echo "${hostname}" > /etc/hostname
hostname ${hostname}

public_ip=$(ec2metadata --public-ipv4 | awk '{print $NF}')
hosted_zone_id=Z2Y0PN68ZS53MY

cat > /tmp/change-batch.json << EOF
{
    "Comment": "Bamboo 6.1.1 server.",
    "Changes": [
        {
            "Action": "UPSERT",
            "ResourceRecordSet": {
                "Name": "${hostname}.",
                "Type": "A",
                "TTL": 300,
                "ResourceRecords": [
                    {"Value": "${public_ip}"}
                ]
            }
        }
    ]
}
EOF

aws route53 change-resource-record-sets --hosted-zone-id ${hosted_zone_id} --change-batch file:///tmp/change-batch.json

systemctl start bamboo
