#!/bin/bash

bamboo_version=6.1.1

cd /usr/local
wget -qO- https://downloads.atlassian.com/software/bamboo/downloads/atlassian-bamboo-${bamboo_version}.tar.gz | tar -zx
ln -s atlassian-bamboo-${bamboo_version} bamboo
useradd --create-home --home-dir /usr/local/bamboo --shell /bin/bash bamboo
chown -R bamboo: atlassian-bamboo-${bamboo_version}/
mkdir /bamboo && chown bamboo: /bamboo
sed -i -e "s/^#bamboo\.home.*/bamboo.home=\/bamboo/" bamboo/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties

cat << EOF > /etc/systemd/system/bamboo.service
[Unit]
Description=Atlassian Bamboo
After=syslog.target network.target

[Service]
Type=forking
User=bamboo
ExecStart=/usr/local/bamboo/bin/start-bamboo.sh
ExecStop=/usr/local/bamboo/bin/stop-bamboo.sh

[Install]
WantedBy=multi-user.target
EOF

cp -a /home/ubuntu/geo-bamboo6/scripts/init_bamboo.sh /usr/local/bin/
cat << EOF > /etc/rc.local
#!/bin/bash
/usr/local/bin/init_bamboo.sh
exit 0
EOF